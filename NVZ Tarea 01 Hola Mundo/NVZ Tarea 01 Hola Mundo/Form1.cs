﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NVZ_Tarea_01_Hola_Mundo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnHola_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "¡Hola mundo de PCP 1-1!";
            lblMensaje.BackColor = Color.MediumOrchid;
        }

        private void btnAdiós_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "¡Adiós mundo de PCP 1-1!";
            lblMensaje.BackColor = Color.FromArgb(83, 26, 109);
            lblMensaje.ForeColor = Color.White;
        }
    }
}
