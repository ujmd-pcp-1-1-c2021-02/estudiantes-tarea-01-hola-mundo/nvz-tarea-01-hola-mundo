﻿
namespace NVZ_Tarea_01_Hola_Mundo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tlpPrincipal = new System.Windows.Forms.TableLayoutPanel();
            this.btnHola = new System.Windows.Forms.Button();
            this.btnAdiós = new System.Windows.Forms.Button();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.tlpPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpPrincipal
            // 
            this.tlpPrincipal.ColumnCount = 2;
            this.tlpPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.7957F));
            this.tlpPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.2043F));
            this.tlpPrincipal.Controls.Add(this.btnHola, 0, 0);
            this.tlpPrincipal.Controls.Add(this.btnAdiós, 0, 1);
            this.tlpPrincipal.Controls.Add(this.lblMensaje, 1, 0);
            this.tlpPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpPrincipal.Location = new System.Drawing.Point(0, 0);
            this.tlpPrincipal.Name = "tlpPrincipal";
            this.tlpPrincipal.RowCount = 2;
            this.tlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPrincipal.Size = new System.Drawing.Size(465, 202);
            this.tlpPrincipal.TabIndex = 0;
            // 
            // btnHola
            // 
            this.btnHola.BackColor = System.Drawing.Color.Indigo;
            this.btnHola.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHola.FlatAppearance.BorderSize = 3;
            this.btnHola.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHola.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHola.ForeColor = System.Drawing.Color.White;
            this.btnHola.Location = new System.Drawing.Point(5, 5);
            this.btnHola.Margin = new System.Windows.Forms.Padding(5);
            this.btnHola.Name = "btnHola";
            this.btnHola.Size = new System.Drawing.Size(96, 91);
            this.btnHola.TabIndex = 0;
            this.btnHola.Text = "HOLA";
            this.btnHola.UseVisualStyleBackColor = false;
            this.btnHola.Click += new System.EventHandler(this.btnHola_Click);
            // 
            // btnAdiós
            // 
            this.btnAdiós.BackColor = System.Drawing.Color.Indigo;
            this.btnAdiós.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAdiós.FlatAppearance.BorderSize = 3;
            this.btnAdiós.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdiós.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdiós.ForeColor = System.Drawing.Color.White;
            this.btnAdiós.Location = new System.Drawing.Point(5, 106);
            this.btnAdiós.Margin = new System.Windows.Forms.Padding(5);
            this.btnAdiós.Name = "btnAdiós";
            this.btnAdiós.Size = new System.Drawing.Size(96, 91);
            this.btnAdiós.TabIndex = 1;
            this.btnAdiós.Text = "ADIÓS";
            this.btnAdiós.UseVisualStyleBackColor = false;
            this.btnAdiós.Click += new System.EventHandler(this.btnAdiós_Click);
            // 
            // lblMensaje
            // 
            this.lblMensaje.BackColor = System.Drawing.Color.Orchid;
            this.lblMensaje.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMensaje.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.Location = new System.Drawing.Point(111, 5);
            this.lblMensaje.Margin = new System.Windows.Forms.Padding(5);
            this.lblMensaje.Name = "lblMensaje";
            this.tlpPrincipal.SetRowSpan(this.lblMensaje, 2);
            this.lblMensaje.Size = new System.Drawing.Size(349, 192);
            this.lblMensaje.TabIndex = 2;
            this.lblMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumPurple;
            this.ClientSize = new System.Drawing.Size(465, 202);
            this.Controls.Add(this.tlpPrincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Hola Mundo PCP 1-1";
            this.tlpPrincipal.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpPrincipal;
        private System.Windows.Forms.Button btnHola;
        private System.Windows.Forms.Button btnAdiós;
        private System.Windows.Forms.Label lblMensaje;
    }
}

